import { inject, provide, computed } from "vue";
import { ethers, providers, Signer } from "ethers";

declare let window: any;
let signer: Signer;

const workspaceSymbol = Symbol();

const connected = computed(() => {
  if (signer != null) {
    return true;
  }
  return false;
});

export const useWorkspace = () => inject(workspaceSymbol);
export const initWorkspace = async () => {
  const provider = new ethers.providers.Web3Provider(window.ethereum);
  await provider.send("eth_requestAccounts", []);
  signer = provider.getSigner();

  provide(workspaceSymbol, {
    providers,
    signer,
    connected,
  });
};
